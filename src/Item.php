<?php

declare(strict_types=1);

namespace ZdenekGebauer\SitemapBuilder;

use DateTimeInterface;

use function in_array;

class Item
{

    /** option for change frequency */
    public const ALWAYS = 'always';

    public const HOURLY = 'hourly';

    public const DAILY = 'daily';

    public const WEEKLY = 'weekly';

    public const MONTHLY = 'monthly';

    public const YEARLY = 'yearly';

    public const NEVER = 'never';

    /**
     * @var string
     */
    private $url;

    /**
     * @var DateTimeInterface
     */
    private $lastMod;

    /**
     * @var string
     */
    private $changeFreq;

    /**
     * @var float
     */
    private $priority = 0.5;

    /**
     * @param string $url absolute url
     * @param DateTimeInterface $lastMod
     * @param string $changeFreq [always|hourly|daily|weekly|monthly|yearly|never]
     * @param float $priority in range 0 - 1
     */
    public function __construct(
        string $url,
        DateTimeInterface $lastMod,
        string $changeFreq = self::MONTHLY,
        float $priority = 0.5
    ) {
        $this->url = $url;
        $this->lastMod = $lastMod;

        $allow = [self::ALWAYS, self::HOURLY, self::DAILY, self::WEEKLY, self::MONTHLY, self::YEARLY, self::NEVER];
        $this->changeFreq = (in_array($changeFreq, $allow, true) ? $changeFreq : self::MONTHLY);

        $this->priority = ($priority > 0 && $priority <= 1 ? $priority : $this->priority);
    }

    public function getUrl(): string
    {
        return $this->url;
    }

    public function getLastMod(): DateTimeInterface
    {
        return $this->lastMod;
    }

    public function getChangeFreq(): string
    {
        return $this->changeFreq;
    }

    public function getPriority(): float
    {
        return $this->priority;
    }
}
