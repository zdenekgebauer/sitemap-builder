<?php

declare(strict_types=1);

namespace ZdenekGebauer\SitemapBuilder;

use LogicException;
use XMLWriter;

/**
 * @see https://www.sitemaps.org/
 */
class Builder extends XMLWriter
{

    private const MAX_ITEMS_PER_FILE = 50000;

    /**
     * @var string
     */
    private $targetDirectory;

    /**
     * @var int
     */
    private $filesCounter = 0;

    /**
     * @var int
     */
    private $itemCounter = 0;

    /**
     * @var bool
     */
    private $plainXml;

    /**
     * @var bool
     */
    private $gzip;

    /**
     * @var ?string
     */
    private $outputFile;

    /**
     * @var string
     */
    private $baseUrl;

    /**
     * @var string
     */
    private $sitemapFileName = 'sitemap%d.xml';

    /**
     * @var array<string>
     */
    private $generatedSitemaps = [];

    /**
     * @param string $directory path directory for generated xml files
     * @param string $baseUrl url to sitemap files
     * @param bool $plainXml create uncompressed plain XML files (sitemap.xml)
     * @param bool $gzip create compressed files (sitemap.xml.gz)
     */
    public function __construct(string $directory, string $baseUrl, bool $plainXml = true, bool $gzip = false)
    {
        $this->targetDirectory = rtrim($directory, '/') . '/';
        $this->baseUrl = $baseUrl;
        $this->plainXml = $plainXml;
        $this->gzip = $gzip;
    }

    /**
     * @deprecated use parameter $gzip in constructor
     */
    public function gzipFiles(bool $gzip): void
    {
        $this->gzip = $gzip;
    }

    /**
     * override default filename of sitemap file (sitemap%d.xml)
     *
     * Placeholder %d will be replaced by number of files.
     * Placeholder can be omitted if total amount of generated items is not greater than 50000.
     *
     * @param string $sitemapFileName
     */
    public function setSitemapFileName(string $sitemapFileName): void
    {
        $this->sitemapFileName = $sitemapFileName;
    }

    private function openFile(): void
    {
        $this->outputFile = (string)tempnam($this->targetDirectory, 'smp');
        file_put_contents($this->outputFile, '');

        $this->openMemory();
        $this->startDocument('1.0', 'UTF-8');
        $this->setIndent(true);
        $this->setIndentString('');
        $this->startElement('urlset');
        $this->writeAttribute('xmlns', 'http://www.sitemaps.org/schemas/sitemap/0.9');
        $this->itemCounter = 0;
    }

    public function addItem(Item $item): void
    {
        if ($this->outputFile === null) {
            $this->openFile();
        }
        $this->startElement('url');
        $this->writeElement('loc', $item->getUrl());
        $this->writeElement('lastmod', $item->getLastMod()->format('c'));
        $this->writeElement('changefreq', $item->getChangeFreq());
        $this->writeElement('priority', (string)$item->getPriority());
        $this->endElement();
        $this->itemCounter++;
        if ($this->itemCounter >= self::MAX_ITEMS_PER_FILE) {
            $this->closeFile();
        }
    }

    private function closeFile(): void
    {
        $this->endElement(); // urlset
        $this->endDocument();
        file_put_contents($this->outputFile, $this->flush(), FILE_APPEND);

        $targetFile = $this->targetDirectory . sprintf($this->sitemapFileName, $this->filesCounter + 1);
        if ($this->plainXml) {
            copy($this->outputFile, $targetFile);
        }

        if ($this->gzip) {
            file_put_contents($targetFile . '.gz', gzencode((string)file_get_contents($this->outputFile), 9));
        }
        unlink($this->outputFile);
        $this->generatedSitemaps[] = $targetFile;
        $this->itemCounter = 0;
        $this->filesCounter++;
        $this->outputFile = null;
    }

    public function close(): void
    {
        if ($this->outputFile !== null) {
            $this->closeFile();
        }

        $countFiles = count($this->generatedSitemaps);
        if ($countFiles > 1) {
            $outputFile = $this->targetDirectory . '/sitemap_index.xml';
            file_put_contents($outputFile, '');
            $this->openMemory();
            $this->startDocument('1.0', 'UTF-8');
            $this->setIndent(true);
            $this->setIndentString('');
            $this->startElement('sitemapindex');
            $this->writeAttribute('xmlns', 'http://www.sitemaps.org/schemas/sitemap/0.9');
            foreach ($this->generatedSitemaps as $sitemapFile) {
                $this->startElement('sitemap');
                $this->writeElement('loc', $this->baseUrl . basename($sitemapFile));
                $this->writeElement('lastmod', date('c', (int)filemtime($sitemapFile)));
                $this->endElement();
            }
            $this->endDocument();
            file_put_contents($outputFile, $this->flush(), FILE_APPEND);
        }
    }

    public function __destruct()
    {
        if ($this->outputFile !== null) {
            throw new LogicException('Method close() have to be called after adding last item');
        }
    }
}
