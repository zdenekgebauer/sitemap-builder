# Sitemap Builder
Generate sitemap files and sitemap index files, regular or gzipped. 
Uses XMLWriter extension for creating XML files.
 
## Usage
```php
use ZdenekGebauer\SitemapBuilder\Builder;
use ZdenekGebauer\SitemapBuilder\Item;

require_once 'vendor/autoload.php';

// create instance, pass path to directory for generated files, base url to this directory
$sitemapBuilder = new Builder(__DIR__, 'http://www.example.org/');
// optionally set required format(s)
// $sitemapBuilder = new Builder(__DIR__, 'http://www.example.org/', true, true);
// optionally set filenames
$sitemapBuilder->setSitemapFileName('sitemap.xml');
// for large sitemaps (>50000 items) filename have to contains placeholder for number  
$sitemapBuilder->setSitemapFileName('custom_%d.xml');

// add items to sitemap
$sitemapBuilder->addItem(new Item('http://www.example.org/', new DateTime('now'), Item::DAILY, 1.0));
$sitemapBuilder->addItem(new Item('http://www.example.org/about', new DateTime('now'), Item::YEARLY, 0.4));
$sitemapBuilder->addItem(new Item('http://www.example.org/page.php?foo=bar', new DateTime('now')));
//...more items

// when all items added, close sitemap
$sitemapBuilder->close();
```
 
