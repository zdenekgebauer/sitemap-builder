<?php

declare(strict_types=1);

namespace ZdenekGebauer\SitemapBuilder;

use DateTime;
use LogicException;

class BuilderTest extends \Codeception\Test\Unit
{

    /**
     * @var \UnitTester
     */
    protected $tester;

    private $testDir = '';

    protected function _before()
    {
        parent::_before();
        $this->testDir = \Codeception\Configuration::dataDir();
    }

    protected function _after()
    {
        // GLOB_BRACE is not available on some systems (alpine linux?)
        $sitemaps = glob($this->testDir . '/*.xml*');
        foreach ($sitemaps as $file) {
            unlink($file);
        }
        $tmpFiles = glob($this->testDir . '/*.tmp');
        foreach ($tmpFiles as $file) {
            unlink($file);
        }
        parent::_after();
    }

    public function testCreateDefaultSitemap(): void
    {
        $builder = new Builder($this->testDir, 'http://example.org/');
        $item = new Item('http://example.org/page1', new \DateTime());
        $builder->addItem($item);
        $builder->close();

        $expectFile = $this->testDir . 'sitemap1.xml';
        $xml = simplexml_load_file($expectFile);
        $this->tester->assertEquals('http://example.org/page1', (string)$xml->url->loc);
        $this->tester->assertStringContainsString(date('Y-m-d\TH:i:'), (string)$xml->url->lastmod);
        $this->tester->assertEquals(Item::MONTHLY, $xml->url->changefreq);
        $this->tester->assertEquals(0.5, (float)$xml->url->priority);

        $this->tester->assertFileNotExists($this->testDir . 'sitemap1.xml.gz');
    }

    public function testCreateDefaultSitemapGzip(): void
    {
        $builder = new Builder($this->testDir, 'http://example.org/', false, true);
        $item = new Item('http://example.org/page1', new \DateTime());
        $builder->addItem($item);
        $builder->close();

        $this->tester->assertFileExists($this->testDir . 'sitemap1.xml.gz');
        $this->tester->assertFileNotExists($this->testDir . 'sitemap1.xml');
    }

    public function testCreateDefaultSitemapBothFormat(): void
    {
        $builder = new Builder($this->testDir, 'http://example.org/', true, true);
        $item = new Item('http://example.org/page1', new \DateTime());
        $builder->addItem($item);
        $builder->close();

        $this->tester->assertFileExists($this->testDir . 'sitemap1.xml.gz');
        $this->tester->assertFileExists($this->testDir . 'sitemap1.xml');
    }

    public function testCreateSitemapWithCustomName(): void
    {
        $builder = new Builder($this->testDir, 'http://example.org/');
        $builder->setSitemapFileName('custom%d.xml');
        $item = new Item('http://example.org/page1', new \DateTime());
        $builder->addItem($item);
        $builder->close();

        $expectFile = $this->testDir . 'custom1.xml';
        $this->tester->assertFileExists($expectFile);
    }

    public function testCreateSitemapIndex(): void
    {
        $builder = new Builder($this->testDir, 'http://example.org/');
        for ($i = 1; $i <= 50001; $i++) {
            $item = new Item('http://www.example.org/' . $i, new DateTime('now'));
            $builder->addItem($item);
        }
        $builder->close();

        $sitemaps = glob($this->testDir . '/sitemap*.xml');
        $this->tester->assertCount(3, $sitemaps);
        $this->tester->assertContains($this->testDir . '/sitemap1.xml', $sitemaps);
        $this->tester->assertContains($this->testDir . '/sitemap2.xml', $sitemaps);

        $expectFile = $this->testDir . 'sitemap_index.xml';
        $xml = simplexml_load_file($expectFile);
        $this->tester->assertEquals('http://example.org/sitemap1.xml', (string)$xml->sitemap[0]->loc);
        $this->tester->assertStringContainsString(
            date('Y-m-d\TH:i:', filemtime($this->testDir . '/sitemap1.xml')),
            (string)$xml->sitemap[0]->lastmod
        );
        $this->tester->assertEquals('http://example.org/sitemap2.xml', (string)$xml->sitemap[1]->loc);
        $this->tester->assertStringContainsString(
            date('Y-m-d\TH:i:', filemtime($this->testDir . '/sitemap2.xml')),
            (string)$xml->sitemap[1]->lastmod
        );
    }

    public function testCloseXml(): void
    {
        $builder = new Builder($this->testDir, 'http://example.org/');
        for ($i = 1; $i <= 10; $i++) {
            $item = new Item('http://example.org/' . $i, new DateTime('now'), Item::DAILY);
            $builder->addItem($item);
        }

        try {
            unset($builder);
        } catch (LogicException $exception) {
            $this->tester->assertEquals(
                'Method close() have to be called after adding last item',
                $exception->getMessage()
            );
        }
    }
}
